require 'tmpdir'
require 'stringio'
require 'timeout'

LINDA=ENV['LINDA']||'linda'

def linda(opt)
  Timeout.timeout(10) {
    return [`#{LINDA} #{opt}`, $?.exitstatus]
  }
end

def after1sec(opt)
  Thread.new do
    sleep 0.1
    linda(opt)
  end
end

def takes(opt)
  start = Time.now
  return linda(opt) + [Time.now - start]
end

RSpec.describe 'Linda' do
  let(:home){ Dir.mktmpdir }
  let(:dat){ "#{home}/.linda/linda.dat" }
  before do
    ENV['HOME'] = home
  end
  describe 'setup' do
    it 'tmpdir' do
      expect(Dir.exists? home).to be true
    end
    it 'env' do
      expect(`env`).to include("HOME=#{home}")
    end
  end

  describe 'singletasks' do
    describe 'out' do
      it 'out' do
        expect(linda('out hoge')).to be == ['',0]
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'out2' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('out fuga')).to be == ['',0]
        expect(File.readlines(dat)).to be == ["hoge\n","fuga\n"]
      end
    end
    describe 'rd' do
      it 'rd' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('rd hoge')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'rd with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('rd ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rdp with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('rdp ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd -p with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('rd -p ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rdp with regexp not match' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('rdp fu.*')).to be == ["",1]
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd -p with regexp not match' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('rd -p fu.*')).to be == ["",1]
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd -p with regexp not exist' do
        expect(linda('rd -p fu.*')).to be == ["",1]
        expect(File.readlines(dat)).to be == []
      end
    end
    describe 'in' do
      it 'in' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('in hoge')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
      it 'in2' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('in hoge')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'in with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('in ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to be == []
      end
      it 'in2 with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('in ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'inp with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('inp ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to be == []
      end
      it 'in -p with regexp' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('in -p ho.*')).to be == ["hoge\n",0]
        expect(File.readlines(dat)).to be == []
      end
      it 'inp with regexp not match' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('inp fu.*')).to be == ["",1]
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'in -p with regexp not match' do
        expect(linda('out hoge')).to be == ['',0]
        expect(linda('in -p fu.*')).to be == ["",1]
        expect(File.readlines(dat)).to be == ["hoge\n"]
      end
      it 'in -p with regexp not exist' do
        expect(linda('in -p fu.*')).to be == ["",1]
        expect(File.readlines(dat)).to be == []
      end
    end
  end

  describe 'multitasks' do
    describe 'rd' do
      it 'rd-out' do
        after1sec('out hoge')
        expect(takes('rd hoge')).to match_array(["hoge\n",0,be > 0.1])
        expect(File.readlines(dat)).to include("hoge\n")
      end
      it 'rd with regexp - out' do
        after1sec('out hoge')
        expect(takes('rd ho.*')).to match_array(["hoge\n",0,be > 0.1])
        expect(File.readlines(dat)).to include("hoge\n")
      end
    end
    describe 'in' do
      it 'in-out' do
        after1sec('out hoge')
        expect(takes('in hoge')).to match_array(["hoge\n",0,be > 0.1])
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
      it 'in with regexp - out' do
        after1sec('out hoge')
        expect(takes('in ho.*')).to match_array(["hoge\n",0,be > 0.1])
        expect(File.readlines(dat)).to_not include("hoge\n")
      end
    end
  end
end

