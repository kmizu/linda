#!/bin/sh
#|
exec csi -s "$0" "$@"
|#
(use srfi-37 posix utils regex srfi-1 posix-extras)

(define (show-usage)
  (print "usage: TODO")
  (exit))

(define (abort msg)
  (print msg)
  (exit))

(define (no-arg-option names processor)
  (option names #f #f processor))

(define options
  (list
    (no-arg-option
      '(#\h "help")
      (lambda _
        (show-usage)))
    (no-arg-option
      '(#\p "non-blocking")
      (lambda (o n a blocking? command text)
        (values #f command text)))))

(define *working-directory*
  ; TODO: Customize
  (make-pathname
    (get-environment-variable "HOME") ".linda"))
(define *lock-file*
  (make-pathname *working-directory* "linda.lock"))
(define *data-file*
  (make-pathname *working-directory* "linda.dat"))
(define *tmp-file*
  (make-pathname *working-directory* "linda.dat.tmp"))


(define (call-with-lock filename thunk)
  (let ((fp (file-open
              filename
              (+ open/wronly open/creat))))
   (file-lock/blocking (open-output-file* fp))
   (let ((rv (thunk)))
    (file-close fp)  
    rv)))

(define (call-with-append-file filename thunk)
  (let* ((fp (file-open
               filename
               (+ open/wronly open/append open/creat)))
         (op (open-output-file* fp #:append)))
    (with-output-to-port op thunk)
    (close-output-port op)))

(define (prepare-repository)
  (create-directory *working-directory* #t)
  (call-with-lock *lock-file* (lambda ()
    (call-with-append-file *data-file* (lambda () #t)))))


(define (out text)
  (prepare-repository)
  (call-with-lock *lock-file* (lambda ()
    (call-with-append-file *data-file* (lambda ()
      (print text))))))

(define (remove-first pred lst)
  ; Returns (values match rest)
  (cond
    ((null? lst)
     (values #f lst))
    ((pred (car lst))
     (values (car lst) (cdr lst)))
    (else
      (receive (found rest) (remove-first pred (cdr lst))
               (values found (cons (car lst) rest))))))

(define (in blocking? reg)
  (prepare-repository)
  (let ((item
          (call-with-lock *lock-file*  (lambda ()
            (let ((pattern (regexp reg)))
              (receive (found rest)
                (remove-first (lambda (l) (string-match pattern l))
                  (read-lines *data-file*))
                (when found
                  (with-output-to-file *tmp-file* (lambda ()
                    (for-each print rest)))
                  (rename-file *tmp-file* *data-file*))
                found))))))
    (if item
      (print item)
      (if blocking?
        (begin
          (sleep 0.1)
          (in blocking? reg))
        (exit 1)))))

(define (rd blocking? reg)
  (prepare-repository)
  (let ((item
          (call-with-lock *lock-file*  (lambda ()
            (let* ((pattern (regexp reg))
                   (item (find (lambda (l) (string-match pattern l))
                               (read-lines *data-file*))))
              item)))))
    (if item
      (print item)
      (if blocking?
        (begin
          (sleep 0.1)
          (rd blocking? reg))
        (exit 1)))))

(receive (blocking? command text)
  (args-fold (command-line-arguments)
             options
             (lambda (o n x . vals)
               (error "Option error: " n))
             (lambda (o blocking? command text)
               (cond
                 ((not command)
                  (values blocking? o text))
                 ((not text)
                  (values blocking? command o))
                 (else
                   (abort "too many arguments"))))
             #t
             #f
             #f)
  (when (not command)
    (show-usage))
  (set! text (or text ""))
  (cond
    ((equal? "out" command) (out text))
    ((equal? "in" command) (in blocking? text))
    ((equal? "rd" command) (rd blocking? text))
    ((equal? "inp" command) (in #f text))
    ((equal? "rdp" command) (rd #f text))
    (else (abort (conc "Unknown command: " command)))))
