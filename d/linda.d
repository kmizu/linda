import core.sys.linux.sys.inotify;
import core.sys.linux.unistd;
import core.thread;
import std.algorithm.searching;
import std.experimental.logger;
import std.file;
import std.getopt;
import std.path;
import std.process;
import std.range;
import std.regex;
import std.stdio;
import std.string;

version (linux) version = use_inotify;

string workingDirectory;
string lockFile;
string dataFile;
string tmpFile;

void initializeFileLocations() {
  workingDirectory = environment.get("LINDA_HOME",
      buildNormalizedPath(environment.get("HOME"), ".linda"));
  info("workingDirectory: ", workingDirectory);
  lockFile = buildNormalizedPath(workingDirectory, "linda.lock");
  info("lockFile: ", lockFile);
  dataFile = buildNormalizedPath(workingDirectory, "linda.dat");
  info("dataFile: ", dataFile);
  tmpFile = buildNormalizedPath(workingDirectory, "linda.dat.tmp");
  info("tmpFile: ", tmpFile);
}

bool nonblocking = false;

bool parseArguments(string[] args, out string command, out string data) {
  info(args);
  auto r = getopt(args,
      "non-blocking|p", "return immediately when there are no data.", &nonblocking,
      "verbose|v","output detail log",function void(){globalLogLevel(LogLevel.all);});
  info(args);
  info(r);
  if(r.helpWanted){
    defaultGetoptPrinter("Usage: ", r.options);
  }
  if (args.length < 2) {
    stderr.writeln("command required");
    return false;
  }
  if (args.length < 3) {
    stderr.writeln("data required");
    return false;
  }
  command = args[1];
  data = args[2];
  return true;
}

void prepareRepository() {
  mkdirRecurse(workingDirectory);
  auto lock = File(lockFile, "w");
  lock.lock(LockType.readWrite);
  auto dat = File(dataFile, "a");
}

void outCommand(string text) {
  info("outCommand");
  prepareRepository();
  auto lock = File(lockFile, "w");
  lock.lock(LockType.readWrite);
  info("lock accuired");
  auto dat = File(dataFile, "a");
  dat.writeln(text);
  info("write: ", text);
}

int maybeWaitUntilSuccess(int delegate() fn) {
  while (true) {
    int ret = fn();
    if (ret == 0 || nonblocking) { return ret; }
    info("sleep");
    version (use_inotify) {
      info("use inotify");
      auto fd = inotify_init();
      assert(fd != -1);
      auto wd = inotify_add_watch(fd, lockFile.toStringz, IN_CLOSE_WRITE | IN_CLOSE_NOWRITE);
      assert(wd != -1);
      ubyte[inotify_event.sizeof + 256] eventBuffer;
      auto bytes = read(fd, eventBuffer.ptr, eventBuffer.length);
      assert(bytes != 0);
      close(fd);
    } else {
      info("use sleep");
      Thread.sleep(dur!"msecs"(10));
    }
    info("wake");
  }
}

int rdCommand(string pattern) {
  info("rdCommand");
  prepareRepository();
  return maybeWaitUntilSuccess(delegate int() {
    auto lock = File(lockFile, "w");
    lock.lock(LockType.readWrite);
    info("lock accuired");
    auto dat = File(dataFile, "r");
    auto r = regex("^"~pattern~"$");
    foreach (line ; dat.byLine()) {
      info("line: ", line);
      if(!matchFirst(line, r).empty()) {
        info("found");
        writeln(line);
        return 0;
      }
    }
    info("not found");
    return 1;
  });
}

int inCommand(string pattern) {
  info("rdCommand");
  prepareRepository();
  return maybeWaitUntilSuccess(delegate int() {
    auto lock = File(lockFile, "w");
    lock.lock(LockType.readWrite);
    info("lock accuired");
    auto dat = File(dataFile, "r");
    auto tmp = File(tmpFile, "w");
    auto r = regex("^"~pattern~"$");
    bool found = false;
    foreach (line ; dat.byLine()) {
      info("line: ", line);
      if (found || matchFirst(line, r).empty()) {
        info("not match");
        tmp.writeln(line);
      } else {
        info("match");
        writeln(line);
        found = true;
      }
    }
    if (found) {
      info("found");
      rename(tmpFile, dataFile);
      info("commit");
      return 0;
    } else {
      info("not found");
      return 1;
    }
  });
}

int main(string[] args) {
  globalLogLevel(LogLevel.error);
  initializeFileLocations();
  string command;
  string data;
  if(!parseArguments(args, command, data)){
    return -1;
  }
  info("linda");
  info("command: ", command);
  info("data: ", data);
  switch (command) {
    case "out":
      outCommand(data);
      break;
    case "rd":
      return rdCommand(data);
    case "rdp":
      nonblocking = true;
      return rdCommand(data);
    case "in":
      return inCommand(data);
    case "inp":
      nonblocking = true;
      return inCommand(data);
    default:
      stderr.writeln("Invalid command");
  }
  return 0;
}
