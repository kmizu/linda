#!/usr/bin/env newlisp
(load "getopts.lsp")
(import "libc.so.6" "flock" "int" "int" "int")

(signal 2 (fn () (exit 1)))

(define *compiled* false)

(define (fix-args) (if *compiled* (main-args) (rest (main-args))))


(define (usage)
  (println "Usage: " ((fix-args) 0) " [options] rd regex")
  (println "       " ((fix-args) 0) " [options] rdp regex")
  (println "       " ((fix-args) 0) " [options] in regex")
  (println "       " ((fix-args) 0) " [options] inp regex")
  (println "       " ((fix-args) 0) " [options] outp regex")
  (dolist (o getopts:short)
    (println (format "\t -%s %-15s\t%s" (o 0) (or (o 1 1) "") (o 1 2))))
  (dolist (o getopts:long)
    (println (format "\t--%s %-15s\t%s" (o 0) (or (o 1 1) "") (o 1 2))))
  (exit 0))

(define version-string "Version: 0.0 (newlisp)")
(define verbosity 1)
(define blocking true)

(define (info)
  (when (>  verbosity 1)
    (apply println (args))))

(shortopt "?" (usage) nil "Print this help message")
(shortopt "h" (usage) nil "Print this help message")
(longopt "help" (usage) nil "Print this help message")

(shortopt "V" (getopts:die version-string) nil "Print version string")
(longopt "version" (getopts:die version-string) nil "Print version string")

(longopt "verbose" (++ verbosity) nil)
(shortopt "v" (++ verbosity) nil "Increase verbosity")

(longopt "quiet" (setq verbosity 0) nil "Quiet")
(shortopt "q" (setq verbosity 0) nil "Quiet")

(longopt "non-blocking" (setq blocking false) nil
         "Failed immediately when no match")
(shortopt "p" (setq blocking false) nil "Failed immediately when no match")

(define largs (getopts (1 (fix-args))))
(info (fix-args))
(info largs)
(info "Verbosity: " verbosity)
(info "Blocking: " blocking)

(define *working-directory* (append (env "HOME") "/.linda"))
(info "*working-directory*: " *working-directory*)
(define *lock-file* (append *working-directory* "/linda.lock"))
(info "*lock-file*: " *lock-file*)
(define *dat-file* (append *working-directory* "/linda.dat"))
(info "*dat-file*: " *dat-file*)
(define *tmp-file* (append *working-directory* "/linda.dat.tmp"))
(info "*tmp-file*: " *tmp-file*)

(when (< (length largs) 2)
  (usage))

(define cmd (largs 0))
(info "cmd: " cmd)
(define text (join (1 largs) " "))
(info "text: " text)

(define (make-sure-linda-directory)
  (when (not (directory? *working-directory*))
    (info "Create repository.")
    (make-dir *working-directory*))
  (with-lock
    (append-file *dat-file* "")))

(define (call-with-lock f)
  (f))

(define-macro (with-lock)
  (let ((with-lock-args (args)))
    (call-with-lock (fn ()
      (dolist (e with-lock-args)
        (eval e))))))
  
(define (linda-output text)
  (make-sure-linda-directory)
  (with-lock
    (append-file *dat-file* (append text "\n"))))

(define (call-with-for-each-line fname f)
  (let ((fdr (open fname "r")))
    (let ((l (read-line fdr)))
    (while l
      (f l)
      (set 'l (read-line fdr))))
    (close fdr)))

(define (linda-read blocking ptn)
  (info "linda-read")
  (make-sure-linda-directory)
  (do-while true
    (info "loop")
    (with-lock
      (call-with-for-each-line *dat-file* (fn (l)
        (info "line:" l)
        (when (regex (append "^" ptn "$") l)
          (info "match!")
          (println l)
          (exit)))))
    (info "no matching line")
    (when (not blocking)
      (info "no retly")
      (exit 1))
    (info "sleep")
    (sleep 100)))

(define (linda-input blocking ptn)
  (info "linda-input")
  (make-sure-linda-directory)
  (do-while true
    (info "loop")
    (with-lock
      (let ((fd (open *tmp-file* "w"))
            (has-match false))
        (call-with-for-each-line *dat-file* (fn (l)
          (info "line: " l)
          (if (and (not has-match) (regex (append "^" ptn "$") l))
            (begin
              (info "match!")
              (println l)
              (set 'has-match true))
            (begin
              (info "keep")
              (write-line fd l)))))
        (close fd)
        (when has-match
          (info "commit!")
          (rename-file *tmp-file* *dat-file*)
          (exit))))
    (info "no matching line")
    (when (not blocking)
      (info "no retly")
      (exit 1))
    (info "sleep")
    (sleep 100)))

(case cmd
  ("rd" (linda-read blocking text))
  ("rdp" (linda-read false text))
  ("in" (linda-input blocking text))
  ("inp" (linda-input false text))
  ("out" (linda-output text))
  (true (usage)))

(exit)
