test: test-newlisp test-d test-scheme

test-newlisp:
	make -C newlisp
	LINDA=./newlisp/linda rspec

test-d:
	make -C d
	LINDA=./d/linda rspec

test-scheme:
	make -C scheme
	LINDA=./scheme/linda rspec

clean:
	make -C newlisp clean
	make -C d clean
	make -C scheme clean
